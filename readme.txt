SVN Revision Graph plugin for IDEs based on IntelliJ Platform 
Version: 1.7
Author:  Dmitry Batrak (dmitry.batrak@gmail.com)
Home page: https://bitbucket.org/batrdmi/svn-rev-graph
Page at JetBrains plugin portal: https://plugins.intellij.net/plugin/?idea&id=6178

SUMMARY
The main purpose of the plugin is to show the revisions that contributed in any way (via copy or merge) 
to current state of file or folder under Subversion control. It is also possible to view the relations 
to the same file(s)/folder(s) present in different branches or tags.

Plugin is available in standard plugin repository and can be installed via IDE's plugin manager. 

LICENSE
Source code is licensed under New BSD License (see license.txt).
Plugin borrows code and ideas from VcsRevisionGraph plugin (http://code.google.com/p/vcsrevisiongraph/),
released under New BSD License. For graph plotting, JGraph library is used (see license-jgraph.txt for its
licensing information). Plugin also uses some code from IntelliJ IDEA Community Edition itself (Apache License v2.0).

ACKNOWLEDGEMENTS
Thanks to adam, eranb, gordon.tyler, opticyclic, Matthias Seemann and Alex Sergeev
for bug reports and improvement suggestions.

DESCRIPTION
Plugin provides the ability to display a revision graph for a project's file or directory managed by Subversion.
The graph shows copy and merge relationships between different revisions. Merge information is displayed in a
simplified way by showing the link from the latest of merged revisions to a target revision.
Current limitations:
1) Only paths that were copied or merged, directly or indirectly, into the path for which graph is built, are shown.
   This means, for example, that if target file or folder was branched some time ago and modified in that branch, but
   never merged back, it won't be shown. It also means that tags are not shown, unless target path is at tag, or there
   was a branch from some tag (in those cases only the specific tag will appear on graph).
2) Merge relationships will be shown correctly only if standard storage layout of modules in Subversion repository is
   used (trunk/branches/tags), merges only happen between trunk, branches and tags, and not within a branch, and changes
   to different branches are not combined in one commit.

First limitation is due to copy information in Subversion available only for copy target, and not for copy
source, so to collect all copy information for a particular file, the scan over all repository revisions is required,
which would be very slow without proper caching, and that is not implemented now. To overcome this limitation to
a certain extent, plugin supports 'extended scan' mode. In this mode it will explicitly search for the paths it
encounters in all branches and tags existing at the moment of the scan. Obviously this will also work only if
the modules in Subversion use standard directories structure.
The second limitation is due to impossibility of fetching mergeinfo information in bulk log operation, it can
be requested from repository only using per-revision requests, which will be very slow. So the plugin uses
indirect method for determining merge sources which will work only for standard directory layout.

USAGE
To display revision graph for selected file or directory, pick 'Revision Graph' item from Subversion submenu.
Operations on selected revision are available either via toolbar buttons or via right-click context menu.
Revision can be selected using both mouse and keyboard (arrow keys).
To view the diff between two revisions, select them both using Ctrl-click first.
Double-click on the link between two revisions will open the diff window for those revisions.
To show the set of revisions that were merged, point the mouse to the corresponding merge link.

CHANGELOG
Version 1.7
    * Version for IDEA 14 (compatibility with previous versions is broken)
    * Darcula UI
    * Revision graph is now available during project indexing
    * Minor UI fixes
Version 1.6.5
    * Rebuilt version 1.6.4 for Java 6
Version 1.6.4
    * Restored compatibility with all IDEs built on IntelliJ platform
Version 1.6.3
    * Rebuilt version 1.6.2 for Java 6 (not all installations of IDEA 12 will use JRE 1.7)
Version 1.6.2
    * Version for IDEA 12 (compatibility with IDEA 11 is broken)
Version 1.6.1
    * Fixed a crash with NPE when history is retrieved from repository with errors
Version 1.6
    * Option to display only revisions contributing to the current one
    * Option to limit 'extended scan' only to branches
    * Fixed UI getting very slow when graph contains a lot of revisions
    * Decreased graph build time (less calls to repository)
    * Project's default scan mode is not updated now if scan using new mode was cancelled
Version 1.5
    * Option to collapse consecutive revisions without links to other branches
    * Some GUI changes to make more clear which branches revisions belong to
    * Fixed minor issue with merged revisions highlighting
Version 1.4
    * Version for IDEA 11 (compatibility with IDEA 10 is broken)
    * Partial workaround for SVN issue 4069
    * Fixed repaint issue after toggling extended scan mode
Version 1.3
    * Ability to display versioned properties and their diffs for graph revisions
    * Gracefully handle pre-1.5 repository versions
Version 1.2
    * Highlighting the set of merged revisions when mouse hovers over merge link
    * Keyboard navigation over revisions in graph
    * Fixed error during processing of revisions with replaced files
    * Fixed error during processing of revisions with both copy and (erroneous) merge information
Version 1.1
    * Graph creation can now be moved to background
    * User will be informed if there were errors while retrieving data from repository
    * Fixed UI freeze before graph window appears
    * Fixed wrong copy source determination in some cases
    * Fixed uncaught exception happening when one double-clicks on graph's empty space
Version 1.0
    * Initial version