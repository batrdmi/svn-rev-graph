package ru.batrdmi.svnplugin.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.ToggleAction;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.util.IconLoader;
import ru.batrdmi.svnplugin.SVNRevisionGraph;

public class CollapseConsecutiveRevisionsAction extends ToggleAction implements DumbAware {
    private final SVNRevisionGraph graph;

    public CollapseConsecutiveRevisionsAction(SVNRevisionGraph graph) {
        super("Collapse All", "Collapse consecutive revisions",
                IconLoader.getIcon("/icons/collapse.png"));
        this.graph = graph;
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        e.getPresentation().setEnabled(!graph.isRefreshInProgress());
    }

    @Override
    public boolean isSelected(AnActionEvent anActionEvent) {
        return graph.isCollapseRevisions();
    }

    @Override
    public void setSelected(AnActionEvent anActionEvent, boolean b) {
        graph.setCollapseRevisions(b);
    }
}
