package ru.batrdmi.svnplugin.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.ToggleAction;
import com.intellij.openapi.project.DumbAware;
import ru.batrdmi.svnplugin.SVNRevisionGraph;
import ru.batrdmi.svnplugin.logic.ScanMode;

public class ScanModeAction extends ToggleAction implements DumbAware {
    private final SVNRevisionGraph graph;
    private final ScanMode thisMode;

    public ScanModeAction(SVNRevisionGraph graph, ScanMode thisMode) {
        super(thisMode == ScanMode.INCLUDE_CURRENT_BRANCHES_AND_TAGS ? "Scan all current branches and tags"
                : thisMode == ScanMode.INCLUDE_CURRENT_BRANCHES ? "Scan all current branches"
                : "Scan only impacting paths");
        this.graph = graph;
        this.thisMode = thisMode;
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        e.getPresentation().setEnabled(!graph.isRefreshInProgress());
    }

    @Override
    public boolean isSelected(AnActionEvent e) {
        return graph.getScanMode() == thisMode;
    }

    @Override
    public void setSelected(AnActionEvent e, boolean state) {
        graph.setScanMode(thisMode);
    }
}
