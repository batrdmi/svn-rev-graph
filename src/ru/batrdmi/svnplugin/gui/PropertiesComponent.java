package ru.batrdmi.svnplugin.gui;

import com.intellij.ui.ScrollPaneFactory;
import com.intellij.ui.table.JBTable;
import com.intellij.util.containers.HashMap;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNPropertyValue;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Map;
import java.util.TreeMap;

// modified version of org.jetbrains.idea.svn.dialogs.PropertiesComponent
public class PropertiesComponent extends JPanel {
    private JTable myTable;
    private JTextArea myTextArea;
    private JSplitPane mySplitPane;

    public PropertiesComponent() {
        init();
    }

    public void init() {
        setLayout(new BorderLayout());
        myTable = new JBTable();
        myTextArea = new JTextArea(0, 0);
        myTextArea.setEditable(false);
        JScrollPane scrollPane = ScrollPaneFactory.createScrollPane(myTable);
        mySplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, scrollPane,
                ScrollPaneFactory.createScrollPane(myTextArea));
        add(mySplitPane, BorderLayout.CENTER);
        final DefaultTableModel model = new DefaultTableModel(createTableModel(new HashMap<String, SVNPropertyValue>()),
                new Object[]{"Name", "Value"}) {
            public boolean isCellEditable(final int row, final int column) {
                return false;
            }
        };
        myTable.setModel(model);
        myTable.setShowVerticalLines(true);
        myTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        myTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                int index = myTable.getSelectedRow();
                if (index >= 0) {
                    SVNPropertyValue value = (SVNPropertyValue) myTable.getValueAt(index, 1);
                    if (value.isString()) {
                        myTextArea.setText(value.getString());
                    } else {
                        myTextArea.setText("");
                    }
                } else {
                    myTextArea.setText("");
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    public void setData(SVNProperties data) {
        final Map<String, SVNPropertyValue> props = new TreeMap<String, SVNPropertyValue>(data.asMap());
        DefaultTableModel model = (DefaultTableModel) myTable.getModel();
        model.setDataVector(createTableModel(props), new Object[]{"Name", "Value"});

        myTable.getColumnModel().setColumnSelectionAllowed(false);
        myTable.getColumnModel().getColumn(1).setCellRenderer(new DefaultTableCellRenderer() {
            protected void setValue(Object value) {
                if (value != null) {
                    if (value.toString().indexOf('\r') >= 0) {
                        value = value.toString().substring(0, value.toString().indexOf('\r')) + " [...]";
                    }
                    if (value.toString().indexOf('\n') >= 0) {
                        value = value.toString().substring(0, value.toString().indexOf('\n')) + " [...]";
                    }
                }
                super.setValue(value);
            }
        });
        mySplitPane.setDividerLocation(.5);
        if (myTable.getRowCount() > 0) {
            myTable.getSelectionModel().setSelectionInterval(0, 0);
        }
    }

    private static Object[][] createTableModel(Map<String, SVNPropertyValue> model) {
        Object[][] result = new Object[model.size()][2];
        int index = 0;
        for (final String name : model.keySet()) {
            result[index][0] = name;
            result[index][1] = model.get(name);
            index++;
        }
        return result;
    }
}
