package ru.batrdmi.svnplugin;

import com.intellij.openapi.components.*;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import ru.batrdmi.svnplugin.logic.ScanMode;

import javax.swing.*;
import java.awt.*;

@State(
  name = SVNRevisionGraphConfiguration.COMPONENT_NAME,
  storages = {@Storage(id = "other", file = "$PROJECT_FILE$"),
              @Storage(id = "dir", file = "$PROJECT_CONFIG_DIR$/other.xml", scheme = StorageScheme.DIRECTORY_BASED)}
)
public class SVNRevisionGraphConfiguration implements ProjectComponent, Configurable,
        PersistentStateComponent<SVNRevisionGraphConfiguration.State> {
    public static SVNRevisionGraphConfiguration getInstance(Project project) {
        return project.getComponent(SVNRevisionGraphConfiguration.class);
    }

    public static final String COMPONENT_NAME = "SVNRevisionGraph.Configuration";
    private State state = new State();
    private JPanel myPanel;
    private JComboBox myComboBox;

    @Override
    public void initComponent() {
    }

    @Override
    public void disposeComponent() {
    }

    @NotNull
    @Override
    public String getComponentName() {
        return COMPONENT_NAME;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "SVN Revision Graph";
    }

    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public void projectOpened() {
    }

    @Override
    public void projectClosed() {
    }

    @Override
    public JComponent createComponent() {
        myPanel = new JPanel(new GridBagLayout());
        myComboBox = new ComboBox(new String[] {"only impacting paths", "include all current branches", "include all current branches and tags"});
        myPanel.add(new JLabel("Scan mode:   "), new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0,0,0,0),0,0));
        myPanel.add(myComboBox, new GridBagConstraints(1,0,1,1,1,0,GridBagConstraints.EAST,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));
        myPanel.add(new JPanel(), new GridBagConstraints(0,1,2,1,1,1,GridBagConstraints.SOUTH,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));
        return myPanel;
    }

    @Override
    public boolean isModified() {
        return myComboBox.getSelectedIndex() != state.scanMode.ordinal();
    }

    @Override
    public void apply() throws ConfigurationException {
        state.scanMode = ScanMode.values()[myComboBox.getSelectedIndex()];
    }

    @Override
    public void reset() {
        myComboBox.setSelectedIndex(state.scanMode.ordinal());
    }

    @Override
    public void disposeUIResources() {
        myComboBox = null;
        myPanel = null;
    }

    public ScanMode getScanMode() {
        return state.scanMode;
    }

    public void setScanMode(ScanMode scanMode) {
        state.scanMode = scanMode;
    }

    public boolean isMergeInfoUnavailableWarningSuppressed() {
        return state.mergeInfoUnavailableWarningSuppressed;
    }

    public void suppressMergeInfoUnavailableWarning() {
        state.mergeInfoUnavailableWarningSuppressed = true;
    }

    public boolean isCollapseRevisions() {
        return state.collapseRevisions;
    }

    public void setCollapseRevisions(boolean b) {
        state.collapseRevisions = b;
    }

    public boolean isDisplayingOnlyImpactingRevisions() {
        return state.displayOnlyImpactingRevisions;
    }

    public void setDisplayOnlyImpactingRevisions(boolean b) {
        state.displayOnlyImpactingRevisions = b;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void loadState(State state) {
        this.state = state;
    }

    public static class State {
        public ScanMode scanMode = ScanMode.ONLY_IMPACTING_PATHS;
        public boolean collapseRevisions;
        public boolean displayOnlyImpactingRevisions;
        public boolean mergeInfoUnavailableWarningSuppressed;
    }
}
