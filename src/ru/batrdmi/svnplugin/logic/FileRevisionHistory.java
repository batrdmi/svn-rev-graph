package ru.batrdmi.svnplugin.logic;

import java.util.Collection;

public class FileRevisionHistory {
    private final String repoRoot;
    private final String relPath;
    private final Revision currentRevision;
    private final boolean isDirectory;
    private final Collection<Revision> allRevisions;
    private final RetrievalStatus status;

    public FileRevisionHistory(Collection<Revision> allRevisions, Revision currentRevision, String repoRoot,
                               String relPath, boolean isDirectory, RetrievalStatus status) {
        this.allRevisions = allRevisions;
        this.currentRevision = currentRevision;
        this.repoRoot = repoRoot;
        this.relPath = relPath;
        this.isDirectory = isDirectory;
        this.status = status;
    }

    public Collection<Revision> getAllRevisions() {
        return allRevisions;
    }

    public Revision getCurrentRevision() {
        return currentRevision;
    }

    public String getRelPath() {
        return relPath;
    }

    public String getRepoRoot() {
        return repoRoot;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public RetrievalStatus getStatus() {
        return status;
    }

    public static class RetrievalStatus {
        public final boolean retrievedWithErrors;
        public final boolean mergeInfoUnavailable;

        public RetrievalStatus(boolean retrievedWithErrors, boolean mergeInfoUnavailable) {
            this.retrievedWithErrors = retrievedWithErrors;
            this.mergeInfoUnavailable = mergeInfoUnavailable;
        }
    }
}
